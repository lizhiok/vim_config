
windows下使用git,如何保存push到远程仓库时使用的密码
方法一

今天在做一个兼容且可跨越的localStorage的实施方案，打算使用github进行项目的代码管理和版本控制。工作环境是Windows，用的git工具是msysgit。

git clone,git commit,git push。这一套下来工作的很流畅，很正常，可是有一点比较麻烦，就是每次push的时候，总是要输入一次在github上面的帐号和密码。就想着能不能在本地存储这个帐号和密码，每次push的时候就自动提交这个去验证。

Google了一下，在一个github的帮助教程的快照上面看到了详细的关于如何在push到远程仓库时，不同系统下面如何去保存验证信息。其中提到了一个软件：git-credential-winstore，这个是专门在Windows下面用来管理git的验证信息的。然后在github的官方帮助文档：上面，找到了软件的官方地址(其实上面那个快照对应的就是下面那个地址的，可能是github更新了，所以内容有些不同，过段时间，Google那边更新快照之后应该就相同了)

可是在安装git-credential-winstore.exe的时候出了问题，会提示无法识别git，软件里面给的解决方式有两种：

    在系统的环境变量PATH里面加上git的安装目录
    通过cmd进行安装 安装使用如下命令行

明确的指定git所在位置

git-credential-winstore.exe -i C:\Program Files(x86)\Git\cmd\git.exe

可惜这两种方式我试过了都失败了。最后在这个文章上才找到解决方式:git-credential-winstore.exe必须放到git的安装目录里面，在bin/文件夹下。

安装这篇文章里面指示的放置文件之后，双击安装，还是报之前的错误。不过幸运的是，通过cmd的方式进行安装成功了。

原因应该是我的环境变量设定有问题吧，不清楚C:\Program Files(x86)\Git\cmd\和C:\Program Files(x86)\Git\bin\应该使用哪一个，这两个目录下面都是有git.exe的。

安装完成之后，就是如何在git里面配置和使用了。

在最开始看的那篇文章里面，说的配置是这样的

git config --global credential.helper cache

作用是启用验证信息缓存功能

可是我按上面的配置之后，在push的时候，却提示：‘credential-cache’ is not a git command

很明显，配置有问题的，可是git实在是新手，只好再次Google之，然后在Stack Overflow上面看到了这个帖子：
说在Windows下面，那个配置中的cache应该改成winstore，正确的命令应该是这样的：

git config --global credential.helper winstore

按照这个命令重新执行了下，果然可以，在第一次push的时候，会有个弹窗，让你输入在github上面的帐号和密码，然后再次push的时候，就不必再输入帐号和密码了。

估计那篇文章里面使用cache，是指的在Linux下的吧，而在Windows下面使用git-credential-winstore来管理验证信息，所以credential.helper就应该是winstore